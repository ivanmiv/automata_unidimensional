** Automata celular unidimensional **

Proyecto vida artificial

Integrantes:

Ivan Miguel Viveros Rayo
Jose Alejandro Libreros



INSTRUCCIONES

En el directorio automata_unidimensional abrir el archivo automata_unidimensional.html (preferiblemente con Google Chrome). 


Se puede ingresar el número de iteraciones deseada,la velocidad de iteraciones (en milisegundos), y seleccionar una de las 256 reglas. Cada que se selecciona una regla de la lista desplegable, aparecerá una descripción de la regla con las reglas de transición asociadas. Se puede "detener", "reiniciar" (limpiar la cuadrícula) e "iniciar" las iteraciones con la regla de transición seleccionada.



Con el botón "Generar estado aleatorio", se genera un estado inicial aleatorio. Puede darse clic después del botón anterior, al botón "iniciar" y se verá el "efecto".

Este proyecto ha sido implementado en Javascript y Bootstrap