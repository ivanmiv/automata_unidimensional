//Selectores JQuery de componentes de la pagina a utilizar
$btn_iniciar = $('#iniciar');
$btn_reiniciar = $('#reiniciar');
$btn_detener = $('#detener');
$btn_estado_aleatorio = $('#estado_aleatorio');
$input_num_iteraciones = $('#numero-iteraciones');
$input_velocidad_iteraciones = $('#velocidad-iteraciones');
$input_regla_seleccionada = $('#regla-seleccionada');
$p_descripcion_regla = $('#descripcion-regla');
$contenedor_automata = $('#contenedor-automata');


// Función que retorna un div con el estado recibido
function obtener_div(estado) {
    color = 'white';
    if (estado == 1) {
        color = 'black'
    }
    return '<div style="width: 1%;height: 1vh; border: 0.5px solid black; background-color: ' + color + '" data-estado="' + estado + '"></div>'
}

// Función que retorna un div con el estado y id recibidos
function obtener_div_id(estado, id) {
    color = 'white';
    if (estado == 1) {
        color = 'black'
    }
    return '<div data-id="' + id + '" style="width: 1%;height: 1vh; border: 1px dotted gray; background-color: ' + color + '" data-estado="' + estado + '"></div>'
}


var automata = {
    reglas: [],
    estado_anterior: [],
    estado_actual: [],
    numero_iteraciones: 100,
    tiempo_iteraciones: 10,
    regla_seleccionada: 1,
    automata_activo: false,
    automata_iniciado: false,
    generar_reglas_wolfram: function () {
        //Función para generar las 256 reglas de wolfram, guardarlas en la matriz de reglas y agregarlas como opción al select de reglas
        select_reglas = "";

        for (let i = 0; i < 256; i++) {
            this.reglas[i] = [];

            for (let j = 0; j < 8; j++) {
                this.reglas[i][j] = (i >> j) & 1;
            }
            this.reglas[i].reverse();
            select_reglas += '<option value=' + i + '>Regla ' + i + '</option>\n'
        }
        $input_regla_seleccionada.html(select_reglas);
        $input_regla_seleccionada.val(1);
    },
    actualizar_descripcion_regla: function (numero_regla) {
        //Función para actualizar los divs que muestran la descripción de la regla gráficamente
        for (i = 0; i < 8; i++) {
            valor = this.reglas[numero_regla][i];

            $descripcion_regla = $('#descripcion-pos-' + i);

            if (valor == 0) {
                $descripcion_regla.removeClass('fondo-negro');
                $descripcion_regla.addClass('fondo-blanco');
            } else if (valor == 1) {
                $descripcion_regla.removeClass('fondo-blanco');
                $descripcion_regla.addClass('fondo-negro');
            }
        }
    },
    obtener_regla_seleccionada: function () {
        // Función para obtener la función que realiza la evaluación de una posición para determinar su nuevo estado a partir de la regla seleccionada
        self = this;
        return function (posicion_evaluar) {

            estado_posicion_anterior = self.estado_anterior[posicion_evaluar - 1];
            estado_posicion_siguiente = self.estado_anterior[posicion_evaluar + 1];
            estado_posicion_evaluar = self.estado_anterior[posicion_evaluar];
            nuevo_estado = 0;

            //Considerando null como el estado quiescente retornamos el mismo valor
            if (estado_posicion_anterior == null || estado_posicion_siguiente == null) {
                return estado_posicion_evaluar;
            }

            if (estado_posicion_anterior == 1 && estado_posicion_evaluar == 1 && estado_posicion_siguiente == 1) {
                nuevo_estado = self.reglas[self.regla_seleccionada][0];
            } else if (estado_posicion_anterior == 1 && estado_posicion_evaluar == 1 && estado_posicion_siguiente == 0) {
                nuevo_estado = self.reglas[self.regla_seleccionada][1];
            } else if (estado_posicion_anterior == 1 && estado_posicion_evaluar == 0 && estado_posicion_siguiente == 1) {
                nuevo_estado = self.reglas[self.regla_seleccionada][2];
            } else if (estado_posicion_anterior == 1 && estado_posicion_evaluar == 0 && estado_posicion_siguiente == 0) {
                nuevo_estado = self.reglas[self.regla_seleccionada][3];
            } else if (estado_posicion_anterior == 0 && estado_posicion_evaluar == 1 && estado_posicion_siguiente == 1) {
                nuevo_estado = self.reglas[self.regla_seleccionada][4];
            } else if (estado_posicion_anterior == 0 && estado_posicion_evaluar == 1 && estado_posicion_siguiente == 0) {
                nuevo_estado = self.reglas[self.regla_seleccionada][5];
            } else if (estado_posicion_anterior == 0 && estado_posicion_evaluar == 0 && estado_posicion_siguiente == 1) {
                nuevo_estado = self.reglas[self.regla_seleccionada][6];
            } else if (estado_posicion_anterior == 0 && estado_posicion_evaluar == 0 && estado_posicion_siguiente == 0) {
                nuevo_estado = self.reglas[self.regla_seleccionada][7];
            }

            return nuevo_estado;
        }
    },
    configuracion_inicial: function () {
        //Función para realizar la configuración inicial del automata
        this.estado_anterior = [];
        this.estado_actual = [];
        this.regla_seleccionada = 1;
        this.automata_activo = false;
        this.automata_iniciado = false;
        if ( !this.numero_iteraciones){
            this.numero_iteraciones = 100;
        }
        if ( !this.numero_iteraciones){
            this.tiempo_iteraciones = 10;
        }
        $btn_iniciar.attr('disabled', false);
        $btn_estado_aleatorio.attr('disabled', false);
        $btn_reiniciar.attr('disabled', true);
        $btn_detener.attr('disabled', true);
        $input_num_iteraciones.val(this.numero_iteraciones);
        $input_velocidad_iteraciones.val(this.tiempo_iteraciones);
        $p_descripcion_regla.html('');
        $contenedor_automata.html('');
        $input_regla_seleccionada.trigger('change');

        for (var i = 0; i < 100; i++) {
            $contenedor_automata.append(obtener_div_id(0, i));
            this.estado_anterior.push(0);
            this.estado_actual.push(0);
        }
    },
    estado_aleatorio: function () {
        //Función para obtener un estado inicial aleatorio

        if (!this.automata_iniciado) {
            $contenedor_automata.html('');
            for (var i = 0; i < 100; i++) {
                estado = (Math.random() * 2 - 0.1) | 0;
                $contenedor_automata.append(obtener_div_id(estado, i));
                this.estado_anterior[i] = estado;
            }
        }
    },
    actualizar_estados: function () {
        //Función para actualizar el automata según el número de iteraciones ingresado
        self = this;
        iteraciones = 1;
        interval = setInterval(actualizar_estado, self.tiempo_iteraciones);

        //Función para actualizar el estado de las celdas del automata
        function actualizar_estado() {
            if (iteraciones > self.numero_iteraciones || !self.automata_activo) {
                clearInterval(interval);
                return;
            }
            var regla = self.obtener_regla_seleccionada(self.regla_seleccionada);

            for (i = 0; i < 100; i++) {
                nuevo_estado = regla(i);
                self.estado_actual[i] = nuevo_estado;
                celda_automata = obtener_div(nuevo_estado);
                $contenedor_automata.append(celda_automata);
            }

            for (j = 0; j < 100; j++) {
                self.estado_anterior[j] = self.estado_actual[j];
            }

            iteraciones += 1;
        }


    },
    iniciar_automata: function () {
        //Función para iniciar el automata, actualiza los elementos de la interfaz y empieza la actualización de estados del automata
        $btn_iniciar.attr('disabled', true);
        $btn_estado_aleatorio.attr('disabled', true);
        $btn_reiniciar.attr('disabled', false);
        $btn_detener.attr('disabled', false);
        this.regla_seleccionada = $input_regla_seleccionada.val();
        this.numero_iteraciones = $input_num_iteraciones.val();
        this.tiempo_iteraciones = $input_velocidad_iteraciones.val();
        if (this.tiempo_iteraciones < 10) {
            this.tiempo_iteraciones = 10;
            $input_velocidad_iteraciones.val(10);
        }
        this.automata_iniciado = true;
        this.automata_activo = true;

        this.actualizar_estados();

    }
};


$btn_iniciar.on('click', function () {
    automata.iniciar_automata();
});

$btn_estado_aleatorio.on('click', function () {
    automata.estado_aleatorio();
});

$btn_reiniciar.on('click', function () {
    automata.configuracion_inicial();
});

$btn_detener.on('click', function () {
    automata.automata_activo = false;
    $btn_detener.attr('disabled', true);
});


$contenedor_automata.on('click', 'div',
    //Función para actualizar el estado inicial del automata
    function () {
        if (!automata.automata_iniciado) {
            posicion = parseInt($(this).data('id'));
            if ($(this).data('estado') == "0") {
                automata.estado_anterior[posicion] = 1;
                $(this).data('estado', "1");
                $(this).css('background-color', 'black');
            } else {
                automata. estado_anterior[posicion] = 0;
                $(this).data('estado', "0");
                $(this).css('background-color', 'white');
            }
        }
    });

$input_regla_seleccionada.on('change', function () {
    automata.actualizar_descripcion_regla($input_regla_seleccionada.val());
});

$input_num_iteraciones.on('change', function () {
    automata.numero_iteraciones = $input_num_iteraciones.val();
});

$input_velocidad_iteraciones.on('change', function () {
    automata.tiempo_iteraciones = $input_velocidad_iteraciones.val();
});

automata.generar_reglas_wolfram();
$input_regla_seleccionada.select2();
automata.configuracion_inicial();

